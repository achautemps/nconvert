# NCONVERT

Readme CMD LINE NConvert

## Ligne de base

```
./nconvert -out jpeg -o  "C:\dev\buildtoolbox\montres\final\7158980\7158980-VE1-1200.jpg" -ratio -rtype lanczos -resize 1200 1200 -canvas 1200 1200 center -q 90 -opthuff -dct 2 -subsampling 2 -rmeta -merge_alpha -bgcolor 255 255 255 "C:\dev\buildtoolbox\montres\diesel\ve1\DZ7377_main.jpg"
```

### Description de la ligne de commande

```js
-out jpeg // image générée au format .jpeg
-o "C:\dev\buildtoolbox\montres\final\7158980\7158980-VE1-1200.jpg" // path de l'image générée
-ratio // on garde le même ratio pour le scaling /!\ à mettre sans se poser de question
-rtype lanczos // resample en Lanczos (à vrai dire je ne sais pas ce que ça fait mais c'est pour Seb) /!\ à mettre sans se poser de question
-resize 1200 1800 // Resize l'image dans l'image à 1200 x 1200
-canvas 1200 1200 center // Créer une image de 1200 x 1200 en centrant l'image d'origine /!\ à faire pour toute la liste de formats
-q 90 // Compression à 90 /!\ à mettre sans se poser de question
-opthuff // Optimisation pour JPEG /!\ à mettre sans se poser de question
-dct 2 -subsampling 2 -merge_alpha // Je ne sais pas trop pour tous ceux là  /!\ à mettre sans se poser de question
-rmeta // Supprime les metas données des images /!\ à mettre sans se poser de question
-bgcolor 255 255 255 // Mets un background blanc dans l'image d'origine si l'image ne prend pas tout le format généré /!\ à mettre sans se poser de question
```


## Suites aux tests sur les formats rectangle (ex : 800 x 1200)  
```
./nconvert -out jpeg -o  "C:\dev\buildtoolbox\montres\final\7158980\7158980-VE1-1200.jpg" -ratio -rtype lanczos -resize 1200 1800 -canvas 1200 1200 center -q 90 -opthuff -dct 2 -subsampling 2 -rmeta -merge_alpha -bgcolor 255 255 255 "C:\dev\buildtoolbox\montres\diesel\ve1\DZ7377_main.jpg"

```
Le travail se fait surtout au niveau du -resize : -resize 1200 1800
En gros j'ai calculé le rapport hauteur / largeur :  ratio = 1200 / 800 === 1.5
Ensuite je prends la largeur de base et la hauteur calculée avec ce ratio  : 1200 * 1.5 = 1800

donc : -resize 1200 1800 

Tu peux donc laisser cette règle même pour les formats carrés  : ratio = 1200 / 1200 === 1

Ce n'est pas facile à expliquer à l'écris mais tu peux m'appeler pour que je t'explique à l'oral si tu ne comprends pas cette partie !


## Ligne finale 

```
./nconvert -out jpeg -o  $pathfinal -ratio -rtype lanczos -resize $format $format*$ratio -canvas $format $format center -q 90 -opthuff -dct 2 -subsampling 2 -rmeta -merge_alpha -bgcolor 255 255 255 $pathImageOrigine

```

```js 
$pathfinal // path de l'image générée
$format // Format de l'image 
$format*$ratio // format * ratio hauteur/largeur de l'image d'origine (voir exemple ci-dessus)
$pathImageOrigine // path de l'image d'origine
```